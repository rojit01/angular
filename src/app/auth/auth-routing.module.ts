import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainAuthComponent } from './main-auth/main-auth.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';


const routes: Routes = [
  {path:'',component:MainAuthComponent,children:[
    {path:'login',component:LoginComponent},
    {path:'signup',component:SignupComponent},
    {path:'',redirectTo:'/signup',pathMatch:'full'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
